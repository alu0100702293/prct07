
require "prct06"

describe Prct06::Referencia do
  before :each do
    @b1 = Prct06::Referencia.new("Dave Thomas", 
											"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide",
											"The Facets of Ruby", "Pragmatic Bookshelf", "4 edition", "July 7, 2013", 
											"ISBN-13: 978-1937785499")
		@b2 = Prct06::Referencia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"], 
											"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide",
											nil, "Pragmatic Bookshelf", "4 edition", "July 7, 2013", 
											["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
		
  end

  it "debe existir almenos un autor" do
    @b1.autor.should_not eq nil
    @b2.autor.should_not eq nil
  end

  it "debe existir un titulo" do
    @b1.titulo.should_not eq nil
  end

  it "debe existir o no una serie" do
    @b1.serie.should_not eq nil 
    @b2.serie.should eq nil
  end

  it "debe existir una editorial" do
    @b1.editorial.should_not eq nil
  end
  
  it "debe existir un numero de edicion" do
    @b1.num_edi.should_not eq nil
  end

  it "debe existir una fecha de publicacion" do
    @b1.fecha_publi.should_not eq nil
  end

  it "debe haber almenos un numero isbn" do
    @b1.isbn.should_not eq nil
  end
  
  #aqui tenemos la comprobacion de los metodos de acceso
  it "Existir un metodo que nos devuelve almenos un autor" do
    @b1.autor.should eq("Dave Thomas") 
    @b2.autor.should eq(["Dave Thomas","Andy Hunt", "Chad Fowler"])
  end

  it "Existe metodo que nos devuelve el titulo" do
    @b1.titulo.should eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide")
  end

  it "Existe un metodo que nos de devuelve o no una serie" do
    @b1.serie.should eq("The Facets of Ruby") 
  end

  it "Existe un metodo que devuelve la editorial" do
    @b1.editorial.should eq("Pragmatic Bookshelf") 
    @b1.num_edi.should eq("4 edition") 
  end

  it "Existe una fecha de publicacion" do
    @b1.fecha_publi.should eq("July 7, 2013") 
  end

  it "Existe un metodo que devuelve almenos un numero isbn" do
    @b1.isbn.should eq("ISBN-13: 978-1937785499")
  end

	it "Existe un metodo de formateo" do
		@b1.to_s.should eq "Dave Thomas\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide\nThe Facets of Ruby\nPragmatic Bookshelf; 4 edition (July 7, 2013)\nISBN-13: 978-1937785499\n"
		@b2.to_s.should eq "Dave Thomas, Andy Hunt, Chad Fowler\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide\n\nPragmatic Bookshelf; 4 edition (July 7, 2013)\nISBN-13: 978-1937785499\nISBN-10: 1937785491\n"
	end

end
