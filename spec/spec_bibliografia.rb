require "prct06"

describe Prct06::Bibliografia do
    before :each do
        @r1 = Prct06::Referencia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"], 
										"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide",
										nil, "Pragmatic Bookshelf", "4 edition", "July 7, 2013", 
										["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
        @r2 = Prct06::Referencia.new("Scott Chacon",
                                        "Pro Git 2009th Edition",
                                        "Pro", "Apress", "2009 edition", "August 27, 2009",
                                        ["ISBN-13: 978-1430218333","ISBN-10: 1430218339"])
        @r3 = Prct06::Referencia.new(["David Flanagan", "Yukihiro Matsumoto"],
                                        "The Ruby Programming Language",
                                        nil, "O’Reilly Media", "1 edition", "February 4, 2008",
                                        ["ISBN-10: 0596516177", "ISBN-13: 978-0596516178"])                                    
        @r4 = Prct06::Referencia.new(["David Chelimsky", "Dave Astels", "Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"],
                                        "The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends (The Facets of Ruby)",
                                        nil, "Pragmatic Bookshelf", "1 edition", "December 25, 2010",
                                        ["ISBN-10: 1934356379", "ISBN-13: 978-1934356371"])  
        @r5 = Prct06::Referencia.new("Richard E. Silverman",
                                        "Git Pocket Guide",
                                        nil, "O’Reilly Media", "1 edition", "August 2, 2013",
                                        ["ISBN-10: 1449325866","ISBN-13: 978-1449325862"])                                          
        @b1= Prct06::Bibliografia.new @r1
        @b1.add(@r2)
    end
    describe "Nodo" do
        
        it "Se crea un nodo" do
            @b1.head.should_not eq nil
        end
        it "El nodo tiene un valor" do
            @b1.head.value.to_s.should eq @r1.to_s
        end
        it "El nodo apunta a su siguiente" do
            @b1.head.next.value.should eq @r2
    end
    end
    it "Se accede al primer elemento de la lista" do
        @b1.head.value.should eq @r1
    end
    it "Se extrae el primer elemento de la lista" do
        @b1.popup.value.should eq @r1 #estraemos el primer elemento
        @b1.head.value.should eq @r2  #Ahora la cabeza debe apuntar al siguiente elemento
    end
    it "Se accede al ultimo elemento de la lista" do
        @b1.tail.value.should eq @r2
    end
    it "Se puede insertar un elemento" do
        @b1.add(@r3)
        @b1.head.next.value.should eq @r2
        @b1.tail.value.should eq @r3
    end
    it "Se pueden insertar varios elementos" do
        @b1.add([@r3,@r4,@r5])
        @b1.head.next.value.should eq @r2
        @b1.head.next.next.value.should eq @r3
        @b1.head.next.next.next.value.should eq @r4
        @b1.head.next.next.next.next.value.should eq @r5
        @b1.tail.value.should eq @r5
    end
end