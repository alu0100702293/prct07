require "prct06/version"

module Prct06

	class Referencia
		attr_accessor :autor, :titulo, :serie, :editorial, :num_edi, :fecha_publi, :isbn

		def initialize (autor, titulo, serie, editorial, num_edi, fecha_publi, isbn)
			@autor = autor
			@titulo = titulo
			@serie = serie
			@editorial = editorial
			@num_edi = num_edi
			@fecha_publi = fecha_publi
		   @isbn = isbn
		end

		def to_s
			salida = String.new ""
			salida += self.autores
			salida +=
			"#{titulo}\n"+
			"#{serie}\n"+
			"#{editorial}; #{num_edi} (#{fecha_publi})\n"
#			"#{isbn}\n"
			salida += self.isbns
			return salida
		end

		def autores
			salida = String.new ""
			if @autor.instance_of? String
				salida = "#{autor}\n"
			elsif @autor.instance_of? Array
				for item in @autor
					if @autor.last == item
						salida += item.to_s+"\n"
					else
						salida += item.to_s+", "
					end
				end
			end
			return salida
		end


		def isbns
			salida = String.new ""
			if @isbn.instance_of? String
				salida = "#{isbn}\n"
			elsif @isbn.instance_of? Array
				for item in @isbn
				salida += item.to_s+"\n"
				end
			end
			return salida
		end
	end
	
		class Bibliografia
		attr_reader  :head, :tail
		
		Nodo = Struct.new :value, :next

		def initialize (referencias)
			if referencias.instance_of? Referencia
				@head = Nodo.new(referencias, nil)
				@tail = nil
			else raise "Debe crearse una clase Referencia y pasarla como parámetro al constructor de esta clase" end
		end
		def add (value)
			if value.instance_of? Referencia #Si se pasa una sola referencia
				if @head == nil
					@head = Nodo.new(value, nil)
					@tail = nil
				else
					if @tail == nil
						@tail = Nodo.new(value, nil)
						@head.next=@tail
					else
						aux = @tail
						@tail = Nodo.new(value, nil)
						aux.next = @tail
					end
				end
			elsif value.instance_of? Array #Si se pasa un array de referencias
				value.each do |i|
					self.add(i)
					
				end
			else raise "Se debe pasar una Referencia o un Array de Referencias"
			end
		
		end
		
		def popup
			aux = self.head
			@head = aux.next
			return aux
		end
		
	end

end
